package sample;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Endzevich on 07.03.2016.
 */
public class Product {

    private StringProperty prodname;
    private IntegerProperty prodid;

    public Product(int prodid,String prodname){

        setprodname(prodname);
        setprodid(prodid);
    }


    public final void setprodname(String value) { prodnameProperty().set(value); }
    public final void setprodid(Integer value) { prodidProperty().set(value); }


    public String getprodname() { return prodnameProperty().get(); }
    public Integer getprodid() { return prodidProperty().get(); }



    public StringProperty prodnameProperty() {
        if (prodname== null)prodname = new SimpleStringProperty(this, "userid");
        return prodname;
    }
    public IntegerProperty prodidProperty() {
        if (prodid == null) prodid = new SimpleIntegerProperty(this, "prodid");
        return prodid;
    }
}
