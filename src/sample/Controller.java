package sample;
import com.sun.javafx.collections.ObservableListWrapper;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;


import javax.swing.*;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class Controller{

    @FXML
    public static TextField pathField;
@FXML private TableView<itemWT> transactionsTable = new TableView<>();
@FXML private TableView<itemWT> viewTable = new TableView<>();
    public static ObservableList<itemWT> transactionsCollection = FXCollections.observableArrayList();
    public static ObservableList<itemWT> viewsCollection = FXCollections.observableArrayList();
    @FXML private TableColumn trnum;
    @FXML private TableColumn userid;
    @FXML private TableColumn prodid;
    @FXML private TableColumn viewID;
    @FXML private TableColumn prodID;
    @FXML private TableColumn userID;
    @FXML public static TextField SupportValue;
    @FXML private Slider SupportSlider;
    @FXML private Slider ConfidenseSlider;
    @FXML public static TextField ConfidenseValue;
    @FXML private Scene apriori;
    @FXML private Button btnrun;


    public void loadButtonClick(){
        JFileChooser choseFile = new JFileChooser("C:\\Users\\Endzevich\\Documents\\sqlite");
        choseFile.setDialogTitle("Chose file with transactions");

        int result = choseFile.showOpenDialog(choseFile.getParent());

        if (result == JFileChooser.APPROVE_OPTION) {
            try {
                File transactionsfile = choseFile.getSelectedFile();
                String path = transactionsfile.getPath();

                String escaped = path.replaceAll("\\\\", "\\\\\\\\");
//                pathField.setText(escaped);
                pathField.setText("C:\\Users\\Endzevich\\Documents\\sqlite\\AprioriDB_v1.1.s3db");

                dbconn.Conn(pathField.getText());


                //dbconn.CreateDB();
                trnum.setCellValueFactory(new PropertyValueFactory<itemWT,Integer>("trnum"));
                userid.setCellValueFactory(new PropertyValueFactory<itemWT,Integer>("userid"));
                prodid.setCellValueFactory(new PropertyValueFactory<itemWT,Integer>("prodid"));

                transactionsCollection.addAll(dbconn.ReadDBtransact());
                viewsCollection.addAll(dbconn.ReadDBview());

               // dbconn.ReadDB();
               // JOptionPane.showMessageDialog(null, data.get(0).gettrnum());
//                dbconn.CreateDB();
                transactionsTable.setItems(dbconn.ReadDBtransact());
                transactionsTable.setItems(transactionsCollection);
                viewID.setCellValueFactory(new PropertyValueFactory<itemWT,Integer>("trnum"));
                userID.setCellValueFactory(new PropertyValueFactory<itemWT,Integer>("userid"));
                prodID.setCellValueFactory(new PropertyValueFactory<itemWT,Integer>("prodid"));
//                viewTable.setItems(dbconn.ReadDBview());
                viewTable.setItems(viewsCollection);





            }catch (Exception e){
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }

    }
    public void SupportSliderChange(){
        SupportValue.setText(String.valueOf(Math.round(SupportSlider.getValue())));
    }
    public void ConfidenseSliderChange(){
        ConfidenseValue.setText(String.valueOf(Math.round(ConfidenseSlider.getValue())));
    }
    public void SupportValueTyped(){
        try{
            SupportSlider.setValue(Math.round(Double.parseDouble(SupportValue.getText())));
        }catch (Exception e){

        }
    }
    public void ConfidenseValueTyped(){
        try{
            ConfidenseSlider.setValue(Math.round(Double.parseDouble(ConfidenseValue.getText())));
        }catch (Exception e){

        }
    }
@FXML
    public void AprioriShow() {






        // Show dialog window
        try {
            Parent root;
            root = FXMLLoader.load(Main.class.getResource("apriori.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.getIcons().add(new Image(getClass().getResource("betl.png").toExternalForm()));
            stage.setScene(scene);

            stage.show();






        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }



    }
}
