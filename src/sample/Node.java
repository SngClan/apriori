package sample;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Endzevich on 13.03.2016.
 */
public class Node {
    public List<String> productsIDs;
    public ArrayList<Node>  links;



    public Node(List<String> prIDs,ArrayList<Node>  lnk){
        this.productsIDs=prIDs;
        this.links=lnk;
    }
    //TODO check null values
    public String getproductID(int i){
        return this.productsIDs.get(i);
    }
    public List<String> getProductsIDs(){
        return this.productsIDs;
    }
    public ArrayList<Node> getLinks(){
        return this.links;
    }
    public Boolean setProductsIDs(String value){
        return this.productsIDs.add(value);
    }
    public Boolean setlink(Node value){
        return this.links.add(value);
    }

}
