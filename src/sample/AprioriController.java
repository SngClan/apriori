package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import javax.swing.text.FlowView;
import javax.swing.text.InternationalFormatter;
import javax.swing.text.StringContent;
import java.io.IOException;
import java.net.URL;
import java.sql.Array;
import java.text.DecimalFormat;
import java.util.*;
import java.util.Arrays;

/**
 * Created by Endzevich on 11.02.2016.
 */
public class AprioriController implements Initializable{

    @FXML public TextField lololo;
    @FXML public TableColumn antecedentCol;
    @FXML public TableColumn consequentCol;
    @FXML private TableView<Rule> rulesTable = new TableView<>();
    public Set<itemWT> data = new HashSet<>();//all transactions
    public Set<Integer> frequentProducts = new HashSet<>();
    public ArrayList<String> frequentProductsStr = new ArrayList<>();// string values of freq prod ids
    public Set<Integer> transactionID = new HashSet<>();
    public HashMap<Integer, ArrayList<Integer>> mainListFrequent = new HashMap<Integer, ArrayList<Integer>>();
   // public Set<String> rules = new HashSet<>();
    public Map<String, itemWT> testMap = new TreeMap<>();// unique transactions
    public float minsupport = Float.valueOf(Controller.SupportValue.getText())/100;
    public float minConfidence = Float.valueOf(Controller.ConfidenseValue.getText())/100;
    public HashSet<Integer> trNumList = new HashSet<>(); //transactions ID's
    int count = 1;
    public HashMap<Integer, ArrayList<ArrayList<String>>> tree = new HashMap<>();

/*
* неучитыватся будут те записи в таблицах, которые совпадают по trnum viewnum и prodid
* так как один и тот же пользователь может несколько раз сделать покупку
*
* */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


        try {

            data.addAll(Controller.transactionsCollection);
            data.addAll(Controller.viewsCollection);
            ArrayList<Product> products = new ArrayList<>(dbconn.ReadDBproduct());//list of products
            List<Integer> transactionProducts = new ArrayList<>();//only products from transactions
            List<Integer> notFrequentProductsList = new ArrayList<>();// products with support
            ObservableList<Rule> rules =  FXCollections.observableArrayList();

            ArrayList<Node> rootElements = new ArrayList<Node>();


            antecedentCol.setCellValueFactory(new PropertyValueFactory<Rule,String>("antecedent"));
            consequentCol.setCellValueFactory(new PropertyValueFactory<Rule,String>("consequent"));
           // Node root = new Node(,null);




            for(itemWT item: data){
              //  System.out.println(item.hashCode());
//                transactionProducts.add(item.getprodid());
                trNumList.add(item.gettrnum());

            }
            System.out.println("All transactions: "+data.size());
            for(itemWT item: Controller.transactionsCollection){
                testMap.put(String.valueOf(item.gettrnum())+String.valueOf(item.getprodid())+String.valueOf(item.getuserid()),item);
            }
            for(itemWT item: Controller.viewsCollection){
                testMap.put(String.valueOf(item.gettrnum())+String.valueOf(item.getprodid())+String.valueOf(item.getuserid()),item);
            }

            System.out.println("Unique transactions: "+testMap.size());
            int treeDepth =0;
            for(Integer id: trNumList){      // trNumList - collection of transaction id's
                ArrayList<Integer> tmpListItems = new ArrayList<>(); // temporary array with unique TransactionID
                for(Map.Entry<String, itemWT> pair : testMap.entrySet()){
                    itemWT value = pair.getValue();
                    if(value.gettrnum()==id)
                        tmpListItems.add(value.getprodid());
                }
                if(tmpListItems.size()>treeDepth) treeDepth = tmpListItems.size();
                mainListFrequent.put(id,tmpListItems);
            }
            trNumList.clear();


            for(Map.Entry<String, itemWT> pair : testMap.entrySet()){
                itemWT value = pair.getValue();
                transactionProducts.add(value.getprodid());
                trNumList.add(value.gettrnum());
            }

            System.out.println("Tree depth: "+treeDepth);


            int size = mainListFrequent.size();
            float maxsupp = 0;

            for (Product item: products) {
                float frequency = Collections.frequency(transactionProducts, item.getprodid());

                float suppItem = frequency/size;
                if(maxsupp<suppItem) maxsupp=suppItem;

                if(suppItem<minsupport){
                    notFrequentProductsList.add(item.getprodid());
                }
            }

            transactionProducts.removeAll(notFrequentProductsList);
            frequentProducts.addAll(transactionProducts);
            for(Integer key: frequentProducts){
               frequentProductsStr.add(key.toString());
            }

            for(Integer key: trNumList){
                ArrayList<Integer> list = mainListFrequent.get(key);
                list.removeAll(notFrequentProductsList);

            }

//            for(String item: frequentProductsStr) {
//                Node a = new Node(new ArrayList<String>(Arrays.asList(item)), null);
//                rootElements.add(a);
//
//            }
            tree = genTree(treeDepth,frequentProductsStr);
            for(int i = 1; i<treeDepth; i++){
               // System.out.println(tree.get(i));
                for(ArrayList item: tree.get(i)){
                    ArrayList<String> consequent = new ArrayList<>();
                    for(int j = 1; j<item.size();j++) consequent.add(item.get(j).toString());
                    rules.add(new Rule(item.get(0).toString(),consequent));
                }
            }
            ArrayList<Rule> rulesForRemoving = new ArrayList<>();
            for(Rule rule: rules){
//                System.out.println(rule.getAntecedent()+" -> "+rule.getConsequent());

                if(calcConfidence(rule,mainListFrequent,trNumList)<minConfidence)
                    rulesForRemoving.add(rule);
            }
            rules.removeAll(rulesForRemoving);
            for(Rule rule: rules){

                for(Product prd: products){

                    if(prd.getprodid().toString().equals(rule.getAntecedent())){
                        rule.setAntecedent(prd.getprodname());
                        //System.out.println(rule.getAntecedent());

                    }
                    ArrayList<String> namePrd = new ArrayList<>();
                    ArrayList<String> idPrd = new ArrayList<>();
                    for(String item: rule.getConsequent()){
                        if(prd.getprodid().toString().equals(item)){
                            namePrd.add(prd.getprodname());
                            idPrd.add(item);
                        }
                    }
                    rule.getConsequent().removeAll(idPrd);
                    rule.setConsequent(namePrd);


                }
            }
            rulesTable.setItems(rules);



        }catch (Exception e)
        {
            System.out.println( e.getMessage());
        }



//
//        list.addAll(data);


    }
    public HashMap<Integer, ArrayList<ArrayList<String>>> genTree( int treeDepth, ArrayList<String> products){
//
        HashMap<Integer, ArrayList<ArrayList<String>>> tree = new HashMap<>();
        int elemCount=0;
        ArrayList<ArrayList<String>> list = new ArrayList<>();
        for(String item: products){
            ArrayList<String> tmp = new ArrayList<String>();
            tmp.add(item);
            list.add(tmp);
        }
        tree.put(0,list);
        for(int i = 1; i<treeDepth; i++){
            ArrayList<ArrayList<String>> newList = new ArrayList<>();
            ArrayList<ArrayList<String>> tmpList =  tree.get(i-1);

            for(String item: products){
                for(ArrayList tmp: tmpList){
                    ArrayList<String> tmp2 = new ArrayList<String>(tmp);

                    if(!tmp.containsAll(Arrays.asList(item))){

                        tmp2.add(item);
                        //System.out.println(findItemset(tmp2, minsupport,mainListFrequent,trNumList));
                        //System.out.println(tmp2);
                        if(findItemset(tmp2, minsupport,mainListFrequent,trNumList)) {
                           // System.out.println("lololol");
                            //System.out.println(tmp);
                            newList.add(tmp2);


                        }else tmp.remove(item);

                           // System.out.println("blabla");
                    }


                }

            }

            tree.put(i,newList);

        }
        return tree;
    }



    public boolean findItemset(ArrayList<String> itemsetStr, float support, HashMap<Integer, ArrayList<Integer>> mainList, HashSet<Integer> trNumList){
        List<Integer> itemset =new ArrayList<>();
        for (String str: itemsetStr){
            itemset.add(Integer.valueOf(str));
        }
        float count = 0;
        float itemsetSupport=0;
        for(Integer id: trNumList){
            ArrayList<Integer> list = mainList.get(id);

            if(list.containsAll(itemset)){
//                System.out.println(itemset+" "+list);
                count++;
            }
        }
        itemsetSupport=count/mainList.size();


        if(itemsetSupport<support) {
            //System.out.println(String.format(itemset + " " + itemsetSupport, "%f%n"));
            return false;
        }
        else {
            //System.out.println(String.format(itemset + " " + itemsetSupport, "%f%n"));
            return true;
        }


        }
    public float calcConfidence(Rule rule, HashMap<Integer, ArrayList<Integer>> mainList, HashSet<Integer> trNumList){
        List<String> itemsetStr =new ArrayList<>(rule.getConsequent());
        List<Integer> itemset =new ArrayList<>();

        for (String str: itemsetStr){
            itemset.add(Integer.valueOf(str));
        }

        int antecedent = Integer.valueOf(rule.getAntecedent());
        itemset.add(antecedent);
        float antecedentSupp = 0;

        float count = 0;
        float countAnt = 0;
        float itemsetSupport=0;
        for(Integer id: trNumList){
            ArrayList<Integer> list = mainList.get(id);
            if(list.contains(antecedent)) countAnt++;
            if(list.containsAll(itemset)){
//                System.out.println(itemset+" "+list);
                count++;
            }
        }
        itemsetSupport=count;


        float confidence=itemsetSupport/countAnt;

        return confidence;


    }



}
