package sample;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Created by Endzevich on 09.02.2016.
 */
public class itemWT {
    private IntegerProperty trnum;
    private IntegerProperty userid;
    private IntegerProperty prodid;

    public itemWT(int trnum, int userid, int prodid){
        settrnum(trnum);
        setuserid(userid);
        setprodid(prodid);
    }
    @Override
    public int hashCode(){
        return Integer.valueOf(String.valueOf(this.gettrnum())+String.valueOf(this.getprodid())+String.valueOf(this.getuserid()));
    }
    @Override
    public String toString(){
        return (String.valueOf(trnum.getValue()+" "+userid.getValue()+" "+prodid.getValue()));
    }

    @Override
    public boolean equals(Object obj) {

        if(obj instanceof itemWT){
            if((this.trnumProperty()==((itemWT)obj).trnumProperty())&&(this.prodidProperty()==((itemWT)obj).prodidProperty())){
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }
    public final void settrnum(Integer value) { trnumProperty().set(value); }
    public final void setuserid(Integer value) { useridProperty().set(value); }
    public final void setprodid(Integer value) { prodidProperty().set(value); }

    public Integer gettrnum() { return trnumProperty().get(); }
    public Integer getuserid() { return useridProperty().get(); }
    public Integer getprodid() { return prodidProperty().get(); }


    public IntegerProperty trnumProperty() {
        if (trnum == null) trnum = new SimpleIntegerProperty(this, "trnum");
        return trnum;
    }
    public IntegerProperty useridProperty() {
        if (userid == null) userid = new SimpleIntegerProperty(this, "userid");
        return userid;
    }
    public IntegerProperty prodidProperty() {
        if (prodid == null) prodid = new SimpleIntegerProperty(this, "prodid");
        return prodid;
    }
}
