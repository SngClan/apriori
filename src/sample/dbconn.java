package sample;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.Date;
import javax.swing.*;

/**
 * Created by Endzevich on 09.02.2016.
 */
public class dbconn {
    public static Connection conn;
    public static Statement statmt;
    public static ResultSet resSet;
    public static Random random = new Random();


    // --------ПОДКЛЮЧЕНИЕ К БАЗЕ ДАННЫХ--------
    public static void Conn(String path)
    {
        try{
            conn = null;
            Class.forName("org.sqlite.JDBC");
        }catch(ClassNotFoundException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        try{
            conn = DriverManager.getConnection("jdbc:sqlite:"+path);
//        C:\Users\Endzevich\Documents\sqlite\testdb.s3db
            // JOptionPane.showMessageDialog(null, "success");

            statmt = conn.createStatement();
        } catch (SQLException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }



    }

    // --------Создание таблицы--------
    public static void CreateDB() throws ClassNotFoundException
    {

            try{
                    statmt.execute("CREATE TABLE if not exists 'Views' ('viewnum' INT, 'userid' INT, 'prodid' INT);");
                    statmt.execute("CREATE TABLE if not exists 'Transactions' ('trnum' INT, 'userid' INT, 'prodid' INT, 'time' TIME);");

                    for(int i=0; i<=100; i++){

                        int j = (random.nextInt(10+1)+1);

                        int user = (random.nextInt(100 + 1)+1);
                        java.sql.Time time = new java.sql.Time(new java.util.Date().getTime());
                        for (int q=1; q<=j; q++){
                            int idPR = random.nextInt(20+1);
                            int prodid = random.nextInt(20+1);
                            statmt.execute("INSERT INTO 'Views' ('viewnum','userid', 'prodid') VALUES ("+i+","+user+","+idPR+"); ");
                            statmt.execute("INSERT INTO 'Transactions' ('trnum','userid', 'prodid', 'time') VALUES ("+i+","+user+","+prodid+",'"+time+"'); ");
                        }

                    }
            } catch (SQLException e){
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
//        try{
//            statmt.execute("CREATE TABLE if not exists 'Products' ( 'prodid' INT, 'name' CHAR);");
//
//            for(int i=0; i<=20; i++){
//
//                    statmt.execute("INSERT INTO 'Products' ( 'prodid', 'name') VALUES ("+i+",'product "+i+"'); ");
//
//
//            }
//        } catch (SQLException e){
//            JOptionPane.showMessageDialog(null, e.getMessage());
//        }
        System.out.println("Таблица создана или уже существует.");
    }

    // --------Заполнение таблицы--------
    public static void WriteDB()
    {
        try {

            statmt.execute("INSERT INTO 'transactions' ('trnum', 'userID', 'prodid') VALUES (231, 1211,21312); ");
        }catch (SQLException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }



    }

    // -------- Вывод таблицы--------
    public static ObservableList<itemWT> ReadDBtransact() throws ClassNotFoundException, SQLException
    {
        //resSet = statmt.executeQuery("SELECT * FROM 'transactions'");
        ObservableList<itemWT> data = FXCollections.observableArrayList();
        resSet = statmt.executeQuery("SELECT * FROM Transactions");

        while(resSet.next())
        {
            int trnum = resSet.getInt("trnum");
            int userid = resSet.getInt("userID");
            int  prodid = resSet.getInt("prodID");

            try{
                data.add(new itemWT(trnum,userid,prodid));
                //JOptionPane.showMessageDialog(null,  data.get(0).getuserid());
            } catch(Exception e){
                JOptionPane.showMessageDialog(null, e.getMessage());
            }

        }

        return data;
    }
    public static ObservableList<itemWT> ReadDBview() throws ClassNotFoundException, SQLException
    {
        //resSet = statmt.executeQuery("SELECT * FROM 'transactions'");
        ObservableList<itemWT> data = FXCollections.observableArrayList();
        resSet = statmt.executeQuery("SELECT * FROM views");

        while(resSet.next())
        {
            int viewnum = resSet.getInt("viewnum");
            int userid = resSet.getInt("userID");
            int  prodid = resSet.getInt("prodID");

            try{
                data.add(new itemWT(viewnum,userid,prodid));
                //JOptionPane.showMessageDialog(null,  data.get(0).getuserid());
            } catch(Exception e){
                JOptionPane.showMessageDialog(null, e.getMessage());
            }

        }
        return data;
    }

    public static ArrayList<Product> ReadDBproduct() throws ClassNotFoundException, SQLException
    {
        //resSet = statmt.executeQuery("SELECT * FROM 'transactions'");
//        ObservableList<itemWT> data = FXCollections.observableArrayList();
        ArrayList<Product> data = new ArrayList<>();
        resSet = statmt.executeQuery("SELECT * FROM Products");

        while(resSet.next())
        {

            int  prodid = resSet.getInt("prodid");
            String prodname = resSet.getString("name");

            try{
                data.add(new Product(prodid, prodname));
                //JOptionPane.showMessageDialog(null,  data.get(0).getuserid());
            } catch(Exception e){
                JOptionPane.showMessageDialog(null, e.getMessage());
            }

        }
        return data;
    }
    public static ArrayList<Integer> getTransaction(Integer id) throws ClassNotFoundException, SQLException
    {
        //resSet = statmt.executeQuery("SELECT * FROM 'transactions'");
//        ObservableList<itemWT> data = FXCollections.observableArrayList();
        ArrayList<Integer> data = new ArrayList<>();

        resSet = statmt.executeQuery("SELECT prodid FROM newTransactions WHERE newTransactions.trnum="+id+"");
        //data=(ArrayList<Integer>)resSet.getArray(resSet.findColumn("prodid"));
        //data = resSet.findColumn("prodid");
//        Array a = resSet.getArray("prodid");
//        String[] nullable = (String[])a.getArray();
       // System.out.println(resSet.toString());
        while(resSet.next())
        {
            int  prodid = resSet.getInt("prodid");

            try{
                data.add(prodid);
                //JOptionPane.showMessageDialog(null,  data.get(0).getuserid());
            } catch(Exception e){
                JOptionPane.showMessageDialog(null, e.getMessage());
            }

        }
        return data;
    }


    public static int getRowsCount(String table){
        int count=0;
        try{

            //statmt = conn.createStatement (ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
           resSet = statmt.executeQuery ("SELECT COUNT(*) AS rowcount FROM(SELECT * FROM '"+table+"' GROUP BY trnum)");

            //statmt = conn.prepareStatement("SELECT * FROM '"+table+"'",ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            //resSet = statmt.executeQuery("SELECT * FROM '"+table+"'");

            resSet.next();
            count = resSet.getInt("rowcount");
            resSet.beforeFirst();
        } catch (SQLException e){
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }

        return count;
    }

    // --------Закрытие--------
    public static void CloseDB() throws ClassNotFoundException, SQLException
    {
        conn.close();
        statmt.close();
        resSet.close();

        System.out.println("Соединения закрыты");
    }




}
