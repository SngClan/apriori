package sample;

import java.util.ArrayList;

/**
 * Created by Endzevich on 15.03.2016.
 */
public class Rule {
    public String antecedent;
    public ArrayList<String> consequent;

    public Rule( String antecedent, ArrayList<String> consequent){
        this.antecedent=antecedent;
        this.consequent=consequent;
    }

    public String getAntecedent(){return this.antecedent;}
    public ArrayList<String> getConsequent(){return this.consequent;}
    public String setAntecedent(String value){return this.antecedent = value;}

    public Boolean setConsequent(ArrayList<String> value){

        return this.consequent.addAll(value);
    }
}
